# Gradle 8.4 完整版快速下载

本仓库提供 Gradle 8.4 完整版（`gradle-8.4-all.zip`）的快速下载链接。Gradle 8.4 版本对基于 JVM 的项目进行了多项改进，包括对 Java 21 的支持、更快的 Java 编译、持久编译器守护进程在 Windows 上的工作，以及更简单的依赖关系配置创建方法。此外，Kotlin DSL 也得到了进一步的改进，Kotlin 版本已更新至 1.9.10，并且简单的属性赋值操作符已提升为稳定。

## 主要改进

- **Java 21 支持**：Gradle 8.4 支持使用 Java 21 编译、测试和运行基于 JVM 的项目。
- **更快的 Java 编译**：在 Windows 上，持久编译器守护进程现在可以正常工作，提升了编译速度。
- **依赖关系配置**：提供了一种更简单的方法来为特定角色创建依赖关系配置。
- **Kotlin DSL 改进**：Kotlin DSL 继续得到改进，Kotlin 版本已更新至 1.9.10，简单的属性赋值操作符已提升为稳定。
- **安全修复**：解决了两个安全漏洞，包括符号链接文件的权限分配问题和通过 XML 外部实体注入可能的本地文本文件渗透问题。

## 下载链接

点击以下链接即可快速下载 `gradle-8.4-all.zip`：

[下载 gradle-8.4-all.zip](链接地址)

## 发行说明

有关 Gradle 8.4 的完整发行说明，请参阅 [Gradle 官方发行说明](https://docs.gradle.org/8.4/release-notes.html)。

## 注意事项

- 请确保您的开发环境支持 Java 21 或更高版本。
- 在 Windows 上使用 Gradle 时，建议使用持久编译器守护进程以提升编译速度。
- 如果您使用 Kotlin DSL，请注意 Kotlin 版本已更新至 1.9.10，并且简单的属性赋值操作符已提升为稳定。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库提供的资源文件遵循 Gradle 的官方许可证。详情请参阅 [Gradle 许可证](https://gradle.org/license/)。